#include "mpi_file_system.h"

void select_ranks(int K, int *rand_index){

    srand(time(NULL));

    /* leader is on NUM_SERVERS-1 position of the array
       left neighbor is on NUM_SERVERS-2 position
       right neighbor is on the 0 position*/

    for(int i=0; i<K/4; i++){
    /* generate index between 1 and NUM_SERVERS-3 */
        rand_index[i] = rand() % (NUM_SERVERS-3) + 1;
    }
}

void find_client_ranks(int *ClientRanks, int *tmp_buffer){

    int size = NUM_SERVERS+NUM_CLIENTS+1;
    int k = 0;
    for(int i=0; i<size; i++){
        if(tmp_buffer[i] != -1){
            ClientRanks[k++] = tmp_buffer[i];
        }
    }
}

/* epistrefei to index tou komvou pou syndeetai amesa o leader kai
    parexei to sydomotero monopati pros ton teliko server */
int direct_channel_shortest_path(int *ServerRanks, bool *direct, int *direct_shortest_dist, int destination){

    int index = -1;
    int min_dist = 0;

    for(int i=0; i<NUM_SERVERS; i++){

        if(direct[i] == true){
            //cout << "server " << ServerRanks[i] << " syndeetai amesa \n";
            int j = i;
            while(ServerRanks[j] != destination){
                min_dist++;
                j--;
                if(j == -1) { min_dist--; j = NUM_SERVERS-1; }
            }
            min_dist++;
            
            if(min_dist < *direct_shortest_dist){
                index = j;
                *direct_shortest_dist = min_dist;
            }
        }
    }  
    return index;
}

bool already_sent_to(int value_position, int *array){

    for(int i=0; i<value_position; i++){
        if(array[i] == array[value_position]) return true;
    }
    return false;
}

void routing_request(int *s_ranks, int file_id, bool *direct, int left, rtype request, int version){

    srand(time(NULL));

    int N = NUM_SERVERS-1; /*afti einai kai h thesi thou leader ston pinaka s_ranks*/
    int rand_index[(N/2)+1], index, dist = 0;
    int message[3]; //0:file_id 1:destination server_rank
    int direct_shortest_dist = NUM_SERVERS-1;

    /* Create custom datatype to store file_id & destination server */
	MPI_Datatype CUSTOM_ARRAY;
	MPI_Type_contiguous(3, MPI_INT, &CUSTOM_ARRAY);
	MPI_Type_commit(&CUSTOM_ARRAY);

    message[0] = file_id;

    for(int i=0; i<(N/2)+1; i++){
    /* generate index between 0 and NUM_SERVERS-2 */
        rand_index[i] = rand() % (NUM_SERVERS-1);

        if(already_sent_to(i, rand_index)){
            i--; continue;
        } 
        message[1] = s_ranks[rand_index[i]];
        message[2] = version;

        if(direct[rand_index[i]] == true){ /* syndeomai amesa me afton ton server */
            //cout << "leader sending request to direct " << s_ranks[rand_index[i]] << endl;
            MPI_Send(&message, 1, CUSTOM_ARRAY, s_ranks[rand_index[i]], request, MPI_COMM_WORLD);

        }else{ /* epilego to syntomotero monopati */

            /* mesw daxtyliou (right neighbour) */
            for(int j=N-1; j>=0; j--){
                dist++;
                if(j == rand_index[j]) break;
            }
            /* mesw kapoias apo tis makrines akmes */
            index = direct_channel_shortest_path(s_ranks, direct, &direct_shortest_dist, message[1]);

            if(dist < direct_shortest_dist) {
                //cout << "leader sending " << request << " request for file " << message[0] <<" to left, with destination " << s_ranks[rand_index[i]] << endl;
                MPI_Send(&message, 1, CUSTOM_ARRAY, left, request, MPI_COMM_WORLD);
            }else{
                if(index >= 0){
                    //cout << "leader sending " << request << " request to direct, with destination " << s_ranks[rand_index[i]] << endl;
                    MPI_Send(&message, 1, CUSTOM_ARRAY, s_ranks[index], request, MPI_COMM_WORLD);
                }else{
                    //cout << "leader sending " << request << " request to left, with destination " << s_ranks[rand_index[i]] << endl;
                    MPI_Send(&message, 1, CUSTOM_ARRAY, left, request, MPI_COMM_WORLD);
                }
            }
        }
    }
    MPI_Type_free(&CUSTOM_ARRAY);
}

int main(int argc, char *argv[]) {

    int rank, size;
    vector<File> server_files;

    // <executable> == argv[0]
    // <NUM_SERVERS> == argn[1]
    // <testfile> == argv[2]

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Status status;

    NUM_SERVERS = atoi(argv[1]);
    NUM_CLIENTS = size-NUM_SERVERS-1;

    int tmp_buffer[size];

    char name[80];
    int length;
    MPI_Get_processor_name(name, &length);

    //cout << "Hello MPI! Rank: " << rank << " Total: " << size <<" Machine: " << name << endl << endl;
    int buffer[3]; //0:left 1:right for server | 0: leader for client
    int dummy_buffer = 0;
    int type = -1; //server or client
    int left_neighbour_rank = -1, right_neighbour_rank = -1;
    int ServerRanks[NUM_SERVERS];
    int ClientRanks[NUM_CLIENTS];

    /** Create custom datatype to send array of MPI_INTs **/
    MPI_Datatype CUSTOM_ARRAY;
    MPI_Type_contiguous(3, MPI_INT, &CUSTOM_ARRAY);
    MPI_Type_commit(&CUSTOM_ARRAY);

    /* fill in with all ranks */
    tmp_buffer[0] = -1;
    for(int i=1; i<size; i++){
        tmp_buffer[i] = i;
    }

    if (rank == 0){
        //printf("[rank: %d] Coordinator started\n", rank);
        //cout << "argv: " << argv[0] <<" "<< argv[1] <<" "<< argv[2] << endl;
        cout << "NUM_SERVERS: " << NUM_SERVERS << endl;
        cout << "NUM_CLIENTS: " << NUM_CLIENTS << endl << endl;

        ifstream inputFileStream(argv[2]);
        int index = 0;
        int leader = -1;
        string line;
        while (!inputFileStream.eof()){

            getline(inputFileStream, line);

            istringstream lineStream(line);
            string event;

            getline(lineStream, event, ' ');

            if(event == "SERVER"){

                string rank; //destination
                string lnrank;
                string rnrank;
                
                getline(lineStream, rank, ' ');
                getline(lineStream, lnrank, ' ');
                getline(lineStream, rnrank, ' ');

                //cout << "--------------------------"<< event <<" "<<rank<<" "<<lnrank<<" "<<rnrank<<"\n";

                const char *destination = rank.c_str();
                const char *left = lnrank.c_str();
                const char *right = rnrank.c_str();

                int dst = atoi(destination);

                ServerRanks[index++] = dst;
                tmp_buffer[dst] = -1;

                buffer[0] = atoi(left);
                buffer[1] = atoi(right);

                /* Blocks until it is safe to modify the application buffer for reuse.
                    Safe does not imply that the data was actually received */
                MPI_Send(&buffer, 1, CUSTOM_ARRAY, dst, SERVER, MPI_COMM_WORLD);

                //cout << "coordinator sent SERVER to " << dst << endl;

                MPI_Recv(&dummy_buffer, 1, MPI_INT, MPI_ANY_SOURCE, ACK, MPI_COMM_WORLD, &status);

            }else if(event == "START_LEADER_ELECTION"){
                //cout<< "-------------------------------- event: " << event <<endl;
                for(int i = 0; i < NUM_SERVERS; i++){
                    MPI_Send(&dummy_buffer, 1, MPI_INT, ServerRanks[i], START_LEADER_ELECTION, MPI_COMM_WORLD);
                    //cout << "sent to " << ServerRanks[i] << endl;
                }
                MPI_Recv(&dummy_buffer, 1, MPI_INT, MPI_ANY_SOURCE, LEADER_ELECTION_DONE, MPI_COMM_WORLD, &status);
                //cout << "[coordinator] leader = " << status.MPI_SOURCE << endl;
                leader = status.MPI_SOURCE;
                buffer[0] = status.MPI_SOURCE;

                find_client_ranks(ClientRanks, tmp_buffer);
                
                for(int i=0; i<NUM_CLIENTS; i++){
                    MPI_Send(&buffer, 1, CUSTOM_ARRAY, ClientRanks[i], CLIENT, MPI_COMM_WORLD);
                    MPI_Recv(&dummy_buffer, 1, MPI_INT, ClientRanks[i], ACK, MPI_COMM_WORLD, &status);
                    //cout << "ACK received from client "<<ClientRanks[i]<<endl;
                }
            }else if(event == "UPLOAD"){

                string client;
                string file;

                getline(lineStream, client, ' ');
                getline(lineStream, file, ' ');

                int client_rank = atoi(client.c_str());
                int file_id = atoi(file.c_str());

                MPI_Send(&file_id, 1, MPI_INT, client_rank, UPLOAD, MPI_COMM_WORLD);
                //cout << "--------------------------" << "UPLOAD " << client << " " << file << " sent\n";

            }else if(event == "RETRIEVE"){

                string client;
                string file;

                getline(lineStream, client, ' ');
                getline(lineStream, file, ' ');

                int client_rank = atoi(client.c_str());
                int file_id = atoi(file.c_str());

                MPI_Send(&file_id, 1, MPI_INT, client_rank, RETRIEVE, MPI_COMM_WORLD);
                //cout << "--------------------------" << "RETRIEVE " << client << " " << file << " sent\n";

            }else if(event == "UPDATE"){

                string client;
                string file;

                getline(lineStream, client, ' ');
                getline(lineStream, file, ' ');

                int client_rank = atoi(client.c_str());
                int file_id = atoi(file.c_str());

                MPI_Send(&file_id, 1, MPI_INT, client_rank, UPDATE, MPI_COMM_WORLD);
                //cout << "--------------------------" << "UPDATE " << client << " " << file_id << " sent\n";

            }else{
                //cout << "unknown event: " << event << endl;
            }
        }
        /* termatismos */
        for(int i=0; i<NUM_CLIENTS; i++){
            MPI_Send(&buffer, 1, CUSTOM_ARRAY, ClientRanks[i], REQUEST_SHUTDOWN, MPI_COMM_WORLD);
        }
        cout << endl;
        for(int i=0; i<NUM_CLIENTS; i++){
            MPI_Recv(&dummy_buffer, 1, MPI_INT, MPI_ANY_SOURCE, SHUTDOWN_OK, MPI_COMM_WORLD, &status);
        }
        MPI_Send(&buffer, 1, CUSTOM_ARRAY, leader, REQUEST_SHUTDOWN, MPI_COMM_WORLD);

        MPI_Recv(&dummy_buffer, 1, MPI_INT, leader, SHUTDOWN_OK, MPI_COMM_WORLD, &status);

    }else{ /* SERVER or CLIENT */
        int source = 0, index = 0, num = 0;
        int leader_id = rank; int dst;
        const int K = NUM_SERVERS-3;
        int rand_index[K/4];

		MPI_Recv(&buffer, 1, CUSTOM_ARRAY, source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if(status.MPI_TAG == SERVER){

            int id = 0, candidacy = 0;
            bool direct_channel = false;

            type = SERVER;
            left_neighbour_rank = buffer[0];
            right_neighbour_rank = buffer[1];
            //printf("[rank: %d] Server received message: [%d, %d] from %d\n", rank, left_neighbour_rank, right_neighbour_rank, status.MPI_SOURCE);
            MPI_Send(&dummy_buffer, 1, MPI_INT, status.MPI_SOURCE, ACK, MPI_COMM_WORLD);

            MPI_Recv(&dummy_buffer, 1, MPI_INT, source, START_LEADER_ELECTION, MPI_COMM_WORLD, &status);
            //cout << rank <<" received START_LEADER_ELECTION from " << source << endl;

            //------------------------------------------------------------------------------------
            if(!candidacy){
                id = rank;
                MPI_Send(&id, 1, MPI_INT, left_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD);
                //printf("[rank: %d] sent CANDIDATE_ID to %d\n", rank, left_neighbour_rank);
                candidacy = 1;
            }

            for(int i=0; i<NUM_SERVERS; i++){
                MPI_Recv(&id, 1, MPI_INT, right_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD, &status);

                if(id > leader_id){ leader_id = id; }
                ServerRanks[index++] = id;

                if(id != rank){
                    MPI_Send(&id, 1, MPI_INT, left_neighbour_rank, CANDIDATE_ID, MPI_COMM_WORLD);
                }
            }
            if(leader_id == rank){

                int message[3]; //0:file_id
                bool direct_channels[NUM_SERVERS] = { false };
                map< int, queue<Request> > records;
                map< int, queue<Request> >::iterator map_it;
                bool first_time = true;

                select_ranks(K, rand_index);

                for(int j=0; j<NUM_SERVERS; j++){

                    if(ServerRanks[j] == leader_id) continue;

                    for(int i=0; i<K/4; i++){
                        dst = ServerRanks[rand_index[i]];
                        MPI_Send(&dst, 1, MPI_INT, ServerRanks[j], CONNECT, MPI_COMM_WORLD);
                        direct_channels[rand_index[i]] = true;
                    }
                }
                for(int i=0; i<K/4; i++){
                    MPI_Recv(&dummy_buffer, 1, MPI_INT, MPI_ANY_SOURCE, ACK, MPI_COMM_WORLD, &status);
                }
                MPI_Send(&dummy_buffer, 1, MPI_INT, 0, LEADER_ELECTION_DONE, MPI_COMM_WORLD);

                /*... waiting for requests ...*/

                while (1){

                    MPI_Recv(&message, 1, CUSTOM_ARRAY, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
                    
                    int file_id = message[0];

                    if(status.MPI_TAG == UPLOAD){

                        Request tmp_request;
                        map_it = records.find(file_id);

                        if(map_it != records.end()){
                            //cout << "UPLOAD: found ["<< map_it->first << "] in map\n";
                            MPI_Send(&message, 1, CUSTOM_ARRAY, status.MPI_SOURCE, UPLOAD_FAILED, MPI_COMM_WORLD);
                        }else{

                            tmp_request.client_id = status.MPI_SOURCE;
                            tmp_request.type = UPLOAD;
                            tmp_request.version = 1;
                            tmp_request.count = ( (NUM_SERVERS-1)/2 ) + 1; //posoi servers epeksergazontai to aitima
                            records[file_id].push(tmp_request);

                            routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, UPLOAD, 0);
                        }

                    }else if(status.MPI_TAG == RETRIEVE){

                        Request tmp_request;
                        map_it = records.find(file_id);

                        if(map_it == records.end()){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, status.MPI_SOURCE, RETRIEVE_FAILED, MPI_COMM_WORLD);
                        }else{
                            //cout << "RETRIEVE: ["<< file_id << "] found in map\n";
                            tmp_request.client_id = status.MPI_SOURCE;
                            tmp_request.type = RETRIEVE;
                            tmp_request.version = 1;
                            tmp_request.count = ( (NUM_SERVERS-1)/2 ) + 1; //posoi servers epeksergazontai to aitima
                            records[file_id].push(tmp_request);

                            /* an einai to monadiko stin oura to ektelw */
                            if(records[file_id].size() == 1){
                                routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, RETRIEVE, 0);
                            }
                        }

                    }else if(status.MPI_TAG == UPDATE){

                        Request tmp_request;
                        map_it = records.find(file_id);

                        if(map_it == records.end() || message[2] == 0){
                            //cout << "send UPDATE_FAILED to client "<<status.MPI_SOURCE << endl;
                            MPI_Send(&message, 1, CUSTOM_ARRAY, status.MPI_SOURCE, UPDATE_FAILED, MPI_COMM_WORLD);
                        }else{
                            tmp_request.client_id = status.MPI_SOURCE;
                            tmp_request.type = UPDATE;
                            tmp_request.version = message[2];
                            tmp_request.count = ( (NUM_SERVERS-1)/2 ) + 1;
                            records[file_id].push(tmp_request);

                            //cout << "leader pushed UPDATE request for file "<<file_id<<endl;

                            /* an einai to monadiko stin oura to ektelw */
                            if(records[file_id].size() == 1){
                                message[1] = 1; //flag_bit
                                //cout << "------------------- send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                            }
                        }

                    }else if(status.MPI_TAG == UPLOAD_ACK){

                        (records[file_id].front().count)--;

                        //cout << "UPLOAD count = "<< records[file_id].front().count <<endl;
                        if(records[file_id].front().count == 0){
                            //cout << "no more servers process the UPLOAD request for file " << file_id << endl;
                            MPI_Send(&message, 1, CUSTOM_ARRAY, records[file_id].front().client_id, UPLOAD_OK, MPI_COMM_WORLD);
                            records[file_id].pop();

                            if( !(records[file_id].empty()) ){

                                if(records[file_id].front().type == UPDATE){
                                    message[1] = 1; //flag_bit
                                    message[2] = records[file_id].front().version;
                                    //cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                    MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                }else{
                                    routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, records[file_id].front().type, 0);
                                }
                            }
                        }
                    }else if(status.MPI_TAG == RETRIEVE_ACK){
                        //cout << "leader received RETRIEVE_ACK from "<<status.MPI_SOURCE << " for file_id "<<file_id << endl;
                        (records[file_id].front().count)--;

                        //cout << "RETRIEVE count = "<< records[file_id].front().count <<endl;
                        records[file_id].front().version = message[2];

                        if(records[file_id].front().count == 0){
                            //cout << "no more servers process the RETRIEVE request for file " << file_id << endl;
                            MPI_Send(&message, 1, CUSTOM_ARRAY, records[file_id].front().client_id, RETRIEVE_OK, MPI_COMM_WORLD);
                            //cout << "file " << file_id << " is retrieved by client "<< records[file_id].front().client_id  << endl;
                            records[file_id].pop();

                            if( !(records[file_id].empty()) ){

                                if(records[file_id].front().type == UPDATE){
                                    message[1] = 1; //flag_bit
                                    message[2] = records[file_id].front().version;
                                    // cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                    MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                }else{
                                    routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, records[file_id].front().type, 0);
                                }
                            }
                        }

                    }else if(status.MPI_TAG == VERSION_CHECK){
                        // cout << "------------------- Leader received version check for file " << message[0] << " version "<< message[2] << endl;
                        
                        int flag_bit = message[1];

                        if(flag_bit == 0){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, records[file_id].front().client_id, VERSION_OUTDATED, MPI_COMM_WORLD);
                            records[file_id].pop();

                            if( !(records[file_id].empty()) ){

                                if(records[file_id].front().type == UPDATE){
                                    message[1] = 1; //flag_bit
                                    message[2] = records[file_id].front().version;
                                    // cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                    MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                }else{
                                    routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, records[file_id].front().type, 0);
                                }
                            }
                        }else if(flag_bit == 1){
                            int version = message[2];
                            routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, UPDATE, version);
                        }
                    }else if(status.MPI_TAG == UPDATE_ACK){

                        (records[file_id].front().count)--;

                        if(records[file_id].front().count == 0){
                            //cout << "no more servers process the UPDATE request for file " << file_id << endl;
                            MPI_Send(&message, 1, CUSTOM_ARRAY, records[file_id].front().client_id, UPDATE_OK, MPI_COMM_WORLD);
                            //cout << "file " << file_id << " is retrieved by client "<< records[file_id].front().client_id  << endl;
                            records[file_id].pop();

                            if( !(records[file_id].empty()) ){

                                if(records[file_id].front().type == UPDATE){
                                    message[1] = 1; //flag_bit
                                    //cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                    MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                }else{
                                    routing_request(ServerRanks, file_id, direct_channels, left_neighbour_rank, records[file_id].front().type, 0);
                                }
                            }
                        }
                    }else if(status.MPI_TAG == REQUEST_SHUTDOWN){

                        if(first_time){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
                            first_time = false;
                        }else{
                            MPI_Send(&dummy_buffer, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
                            break;
                        }
                    }else{
                        cout << "unknown request: " << status.MPI_TAG << endl;
                    }
                }
                
            }else{ /* SERVER (not leader) */

                int message[3]; //an den proorizetai gia mena to proothw aristera
                vector<File> myfiles;
                File tmp_file;
                int file_id;
            
                for(int i=0; i<K/4; i++){
                    MPI_Recv(&dst, 1, MPI_INT, leader_id, CONNECT, MPI_COMM_WORLD, &status);

                    if(dst == rank){ /*leader wants to create a direct channel with me*/
                        direct_channel = true;
                        MPI_Send(&dummy_buffer, 1, MPI_INT, leader_id, ACK, MPI_COMM_WORLD);

                        cout << rank << " CONNECTED TO " << leader_id << endl;
                    }
                }

                while(1){

                    MPI_Recv(&message, 1, CUSTOM_ARRAY, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                    if(status.MPI_TAG == UPLOAD){

                        if(message[1] != rank){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPLOAD, MPI_COMM_WORLD);
                        }else{

                            tmp_file.id = message[0];
                            tmp_file.version = 1;
                            myfiles.push_back(tmp_file);

                            //cout << rank << " sending UPLOAD_ACK to leader for file_id " << message[0] << endl;
                            if(direct_channel){
                                MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPLOAD_ACK, MPI_COMM_WORLD);
                            }else{
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPLOAD_ACK, MPI_COMM_WORLD);
                            }
                        }
                    }else if(status.MPI_TAG == UPLOAD_ACK){
                        
                        if(direct_channel){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPLOAD_ACK, MPI_COMM_WORLD);
                        }else{
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPLOAD_ACK, MPI_COMM_WORLD);
                        }
                    }else if(status.MPI_TAG == RETRIEVE){

                        if(message[1] != rank){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, RETRIEVE, MPI_COMM_WORLD);
                        }else{
                            message[2] = 0; //version
                            // cout << "\nfile vector of server "<< rank << ": ";
                            for (auto i = myfiles.begin(); i != myfiles.end(); ++i){
                                // cout << (*i).id << " ";
                                if((*i).id == message[0]){
                                    //na periexei to version!!
                                    message[2] = (*i).version;
                                }
                            }
                            //cout << endl<< rank << " sending RETRIEVE_ACK to leader for file_id " << message[0] << " with version "<< message[2] << endl;
                            if(direct_channel){
                                MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, RETRIEVE_ACK, MPI_COMM_WORLD);
                            }else{
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, RETRIEVE_ACK, MPI_COMM_WORLD);
                            }
                        }
                    }else if(status.MPI_TAG == RETRIEVE_ACK){
                        
                        if(direct_channel){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, RETRIEVE_ACK, MPI_COMM_WORLD);
                        }else{
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, RETRIEVE_ACK, MPI_COMM_WORLD);
                        }
                    }else if(status.MPI_TAG == VERSION_CHECK){
                        int flag_bit = message[1];
                        bool found = false;

                        if(flag_bit == 1){
                            vector<File>::iterator i;
                            for(i = myfiles.begin(); i != myfiles.end(); ++i){
                                if((*i).id == message[0]){
                                    found = true;
                                    if((*i).version <= message[2]){
                                        // cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                        //cout << "old version: "<<(*i).version;
                                        MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                    }else{
                                        message[1] = 0; //flag_bit
                                        // cout << "send VERSION_CHECK for [file_id: "<<message[0]<<", flag_bit: "<<message[1]<<", version: "<<message[2]<<"]\n";
                                        // cout << "old version: "<<(*i).version << endl;
                                        if(direct_channel){
                                            MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, VERSION_CHECK, MPI_COMM_WORLD);
                                        }else{
                                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                                        }
                                    }
                                    break;
                                }
                            }
                            if(!found){ /* to arxeio den vrethike */
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                            }
                            
                        }else if(flag_bit == 0){
                            if(direct_channel){
                                MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, VERSION_CHECK, MPI_COMM_WORLD);
                            }else{
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, VERSION_CHECK, MPI_COMM_WORLD);
                            }
                        }

                    }else if(status.MPI_TAG == UPDATE){

                        bool found = false;

                        if(message[1] != rank){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPDATE, MPI_COMM_WORLD);
                        }else{
                            //cout << "server " << rank << " received UPDATE "<< message[0]<<endl;
                            vector<File>::iterator i;
                            for (i = myfiles.begin(); i != myfiles.end(); ++i){
                                if((*i).id == message[0]){
                                    found = true;
                                    break;
                                }
                            }
                            if(!found){
                                tmp_file.id = message[0];
                                tmp_file.version = message[2]+1;
                                myfiles.push_back(tmp_file);
                                // cout << "------------------- pushed file\n";
                            }else{
                                if((*i).version <= message[2]){
                                    // cout << "------------------- update version\n";
                                    (*i).version = message[2]+1;
                                }
                            }
                            if(direct_channel){
                                MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPDATE_ACK, MPI_COMM_WORLD);
                            }else{
                                MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPDATE_ACK, MPI_COMM_WORLD);
                            }
                        }
                    }else if(status.MPI_TAG == UPDATE_ACK){

                        if(direct_channel){
                            MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPDATE_ACK, MPI_COMM_WORLD);
                        }else{
                            MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, UPDATE_ACK, MPI_COMM_WORLD);
                        }

                    }else if(status.MPI_TAG == REQUEST_SHUTDOWN){
                        MPI_Send(&message, 1, CUSTOM_ARRAY, left_neighbour_rank, REQUEST_SHUTDOWN, MPI_COMM_WORLD);
                        break;
                    }
                }
            }
        }else if(status.MPI_TAG == CLIENT){

            int message[3]; //0:file_id 
            vector<File> myfiles;
            File tmp_file;
            bool id_already = false;
            int active_requests = 0;
            bool shutdown_request = false;

            type = CLIENT;
            leader_id = buffer[0];

            //cout << "[client] leader = " << leader_id << endl;
            MPI_Send(&dummy_buffer, 1, MPI_INT, source, ACK, MPI_COMM_WORLD);

            while(1){
                MPI_Recv(&message, 1, CUSTOM_ARRAY, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

                if(status.MPI_TAG == UPLOAD){
                    tmp_file.id = message[0];
                    tmp_file.version = 1;
                    myfiles.push_back(tmp_file);

                    active_requests++;
                    //cout << "active requests: " << active_requests << endl;
                    MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPLOAD, MPI_COMM_WORLD);
                
                }else if(status.MPI_TAG == UPLOAD_FAILED){
                    cout << "CLIENT " << rank << " FAILED TO UPLOAD " << message[0] << endl;

                }else if(status.MPI_TAG == UPLOAD_OK){                  
                    cout << "CLIENT " << rank << " UPLOADED " << message[0] << endl;

                }else if(status.MPI_TAG == RETRIEVE){
                    active_requests++;
                    //cout << "active requests: " << active_requests << endl;
                    MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, RETRIEVE, MPI_COMM_WORLD);

                }else if(status.MPI_TAG == RETRIEVE_FAILED){
                    cout << "CLIENT " << rank << " FAILED TO RETRIEVE " << message[0] << endl;

                }else if(status.MPI_TAG == RETRIEVE_OK){
                    cout << "CLIENT " << rank << " RETRIEVED VERSION " << message[2] << " OF "<< message[0] << endl;

                    for (auto i = myfiles.begin(); i != myfiles.end(); ++i){
                        if((*i).id == message[0]){ // file_id already exists
                            id_already = true;
                            (*i).version = message[2];
                            break;
                        }
                    }
                    if(!id_already){
                        tmp_file.id = message[0];
                        tmp_file.version = message[2];
                        myfiles.push_back(tmp_file);
                    }
                }else if(status.MPI_TAG == UPDATE){
                    
                    id_already = false;
                    //cout << "------------------- UPDATE received from client " << rank <<" for file "<<message[0] <<endl;
                    
                    for(auto i = myfiles.begin(); i != myfiles.end(); ++i){

                        if((*i).id == message[0]){
                            id_already = true;
                            message[2] = (*i).version;
                            break;
                        }
                    }
                    if(id_already){
                        //cout << "------------------- file_id "<< message[0] << " found in client " << rank << " vector\n"; 
                    }else{
                        message[2] = 0;
                        //cout << "file_id "<< message[0] << " NOT found in client " << rank << " vector\n";
                    }
                    active_requests++;
                    //cout << "active requests: " << active_requests << endl;
                    MPI_Send(&message, 1, CUSTOM_ARRAY, leader_id, UPDATE, MPI_COMM_WORLD);

                }else if(status.MPI_TAG == UPDATE_FAILED){

                    cout << "CLIENT " << rank << " FAILED TO UPDATE " << message[0] << endl;

                }else if(status.MPI_TAG == UPDATE_OK){
                    
                    for (auto i = myfiles.begin(); i != myfiles.end(); ++i){
                        if((*i).id == message[0]){
                            (*i).version++;
                            break;
                        }
                    }
                    cout << "CLIENT " << rank << " UPDATED " << message[0] << endl;

                }else if(status.MPI_TAG == VERSION_OUTDATED){
                    cout << "CLIENT " << rank << " CANNOT UPDATE " << message[0] << " WITHOUT MOST RECENT VERSION\n";
                
                }else if(status.MPI_TAG == REQUEST_SHUTDOWN){

                    //cout << "active requests: " << active_requests << endl;
                    if(active_requests == 0){
                        MPI_Send(&dummy_buffer, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
                        break;
                    }else{
                        shutdown_request = true;
                    }
                }
                if(status.MPI_SOURCE == leader_id){
                    active_requests--; 
                    //cout << "active requests: " << active_requests << endl;
                    if(active_requests == 0){
                        MPI_Send(&dummy_buffer, 1, MPI_INT, 0, SHUTDOWN_OK, MPI_COMM_WORLD);
                        break;
                    }
                }
            }
        }
    }
    //cout << "rank " << rank << " exiting....\n";
    MPI_Type_free(&CUSTOM_ARRAY);
    MPI_Finalize();

    return 0;
}