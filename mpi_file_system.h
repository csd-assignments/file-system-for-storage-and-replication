#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <map>
#include <queue>
#include <string>
#include <time.h>
#include <mpi.h>

#define ACK 0
#define SERVER 1
#define CLIENT 2
#define START_LEADER_ELECTION 3
#define CANDIDATE_ID 4
#define CONNECT 5
#define LEADER_ELECTION_DONE 6

#define UPLOAD_FAILED 10
#define UPLOAD_ACK 11
#define UPLOAD_OK 12

#define RETRIEVE_FAILED 13
#define RETRIEVE_ACK 14
#define RETRIEVE_OK 15

#define UPDATE_FAILED 16
#define UPDATE_ACK 17
#define UPDATE_OK 18
#define VERSION_CHECK 19
#define VERSION_OUTDATED 20

#define REQUEST_SHUTDOWN 21
#define SHUTDOWN_OK 22

using namespace std;

int NUM_SERVERS, NUM_CLIENTS;

enum rtype { UPLOAD = 7, UPDATE = 8, RETRIEVE = 9 };

/* each client/server has a vector of files */
typedef struct file {
    int id;
    int version;
}File;

/* each file has its request queue with all the requests made on this file */
typedef struct request {
    int client_id;
    int count; //number of servers that haven't complete the request
    rtype type;
    int version; //max version in the system
}Request;

/* only the leader has a map container of file records */
typedef struct record {
    int key; //file id
    Request *front, *back;
}Record;